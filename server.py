import foursquare
from flask import Flask, jsonify, request, send_from_directory
from microsofttranslator import Translator

FOURSQUARE_CLIENT_ID = '10I2SCND4ZA40THVSHDNNYDGNVM1J3YDOPO2FBDL2YIYVCQS'
FOURSQUARE_CLIENT_SECRET = 'EEVPEVULC2S34PBTAQI4N2XMBXQRGEO4IP1DZR0USERXEWB1'
MS_TRANSLATOR_CLIENT_ID = 'jajanengendi'
MS_TRANSLATOR_CLIENT_SECRET = 'q3dG2xsFxmZ5gl2JM+2U06kQvW+K7YW6dPFHzt0NlZM='


# set the project root directory as the static folder
app = Flask(__name__, static_url_path='')

@app.route('/')
def root():
    return app.send_static_file('index.html')


@app.route('/css/<path:path>')
def send_css(path):
    return send_from_directory('static/css', path)


@app.route('/js/<path:path>')
def send_js(path):
    return send_from_directory('static/js', path)


@app.route('/api/recommendations/', methods=['GET'])
def recommendations():
    lat = request.args.get('lat')
    lng = request.args.get('lng')
    result = explore_venues(lat, lng)
    if result['totalResults'] == 0:
        return jsonify(message=result['warning']['text'], totalVenues=0)

    venues = parse_venues(result)
    data = {'venues': venues, 'totalVenues': result['totalResults']}
    return jsonify(**data)


@app.route('/api/tips/', methods=['GET'])
def venue_tips():
    venue_id = request.args.get('venue_id')
    venue_result = get_venue_detail(venue_id)
    translated_tips = translate_venue_tips(venue_result)

    data = {'tips': translated_tips, 'totalTips': len(translated_tips)}
    return jsonify(**data)


def get_foursquare_client():
    return foursquare.Foursquare(
        client_id=FOURSQUARE_CLIENT_ID,
        client_secret=FOURSQUARE_CLIENT_SECRET,
    )


def explore_venues(lat, lng):
    client = get_foursquare_client()
    lat_lng = '%s,%s' % (lat, lng)
    return client.venues.explore(
        params={'ll': lat_lng, 'section': 'food', 'venuePhotos': 1, 'openNow': 1}
    )


def parse_venues(result):
    venues = []
    for group in result['groups']:
        for item in group['items']:
            venue = {}
            venue['id'] = item['venue']['id']
            venue['name'] = item['venue']['name']
            venue['location'] = item['venue']['location']
            if 'price' in item['venue']:
                venue['price'] = item['venue']['price']['message']
            if 'rating' in item['venue']:
                venue['rating'] = item['venue']['rating']
            venues.append(venue)

    return venues


def get_venue_detail(venue_id):
    client = get_foursquare_client()
    return client.venues(venue_id)


def translate_venue_tips(result):
    all_tips = []
    non_english_tips = []

    for tip in result['venue']['tips']['groups'][0]['items']:
        if tip.get('lang', 'id') == 'en':
            all_tip = {}
            all_tip['translated_text'] = tip['text']
            all_tips.append(all_tip)
        else:
            non_english_tips.append(tip['text'])

    translator = get_translator()
    translated_tips = translator.translate_array(non_english_tips, 'en')

    for tip in translated_tips:
        all_tip = {}
        all_tip['translated_text'] = tip['TranslatedText']
        all_tips.append(all_tip)

    return all_tips


def get_translator():
    return Translator(MS_TRANSLATOR_CLIENT_ID, MS_TRANSLATOR_CLIENT_SECRET)


if __name__ == "__main__":
    app.run()
